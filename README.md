# Cashier-bot

A Discord cashier bot game.

## How to play

Increment a counter of coins to add a coin.

Example:

- User1 send "1" (it add 1 coin)
- User2 send "2" (it add 1 coin)
- The count is now 2

### Rules

1. To add a coin, you have to increment the counter
2. The counter cannot be incremented twice in a row by the same user
3. The number of coins you can add is limited per day
4. At the beginning of each week, the **top of contributors** is displayed and all
added coins are reset but not the counter
5. The game is played only on one text channel

### Commands

- `/rules` : Display the rules and commands
- `/coins` : Display your remaining coins
- `/top` : Display the top contributors
- `/count` : Display the current counter
- `/set-channel` : (Admin only) Set which text channel the bot should listen

## Installation

```sh
# Install dependencies
yarn
# or
npm i
```

### Environment variables

```sh
COIN_LIMIT=5 # Maximum number of coin per day
DISCORD_CLIENT_ID=CLIENT_ID # Discord bot client ID
DISCORD_TOKEN=TOKEN # Discord bot token
REPORT_DISCORD_USER_ID=USER_ID # Send error report to this Discord user ID
DISCORD_DEV_GUILD_ID=GUILD_ID # (dev) Discord guild ID to deploy commands on dev env
```

Use a PostgreSQL database

```sh
DB_USER=user
DB_PASSWORD=password
DB_HOST=127.0.0.1
DB_PORT=1234
DB_NAME=name
# or
DATABASE_URL=postgres://<DB_USER>:<DB_PASSWORD>@<DB_HOST>:<DB_PORT>/<DB_NAME>
```

### Start the bot

```sh
yarn start
# or
npm start
```
