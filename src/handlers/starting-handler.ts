import { reportError } from "../report-error";
import { reseCoinsRoutine } from "../routines/reset-coins-routine";
import { resetLimitRoutine } from "../routines/reset-limit-routine";
import type { Client } from "discord.js";

export const dailyDelay = 86400000; // A day

const getResetTime = (now = new Date()) =>
    new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate() + 1, // The next day
        3, // hours
        0, // minutes
        0 // seconds
    );

const runRoutines = async (client: Client) => {
    try {
        await reseCoinsRoutine(client)?.catch((err) => reportError(client, err));
        await resetLimitRoutine().catch((err) => reportError(client, err));
    } catch (err) {
        reportError(client, err as Error);
    }
};

export const startingHandler = (client: Client) => {
    const now = new Date();
    const resetTime = getResetTime(now);
    try {
        setTimeout(async () => {
            await runRoutines(client);
            setInterval(() => {
                return runRoutines(client);
            }, dailyDelay);
        }, resetTime.getTime() - now.getTime());
    } catch (err) {
        reportError(client, err as Error);
    }
};
