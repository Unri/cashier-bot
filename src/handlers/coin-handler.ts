import { config } from "dotenv";
import { BankModel, GuildModel } from "../db/models";
import type { Message } from "discord.js";

config();

const COIN_LIMIT = Number(process.env.COIN_LIMIT);

const addCoin = async ({
    guildId,
    authorId,
    channelId,
}: {
    guildId: string;
    authorId: string;
    channelId: string;
}) => {
    const bank =
        (await BankModel.findOne({
            where: {
                guildId,
                userId: authorId,
            },
        })) ??
        (await BankModel.create({
            guildId,
            userId: authorId,
        }));

    if (bank.getDataValue("limitedCount") >= COIN_LIMIT) {
        throw new Error("You reached the daily limit of :coin:");
    }

    const guild =
        (await GuildModel.findByPk(guildId)) ??
        (await GuildModel.create({
            id: guildId,
            counterChannelId: channelId,
            lastContributorId: authorId,
        }));

    guild.update({
        count: guild.getDataValue("count") + 1,
        lastContributorId: authorId,
    });

    const updatedBank = await bank.update({
        coins: bank.getDataValue("coins") + 1,
        limitedCount: bank.getDataValue("limitedCount") + 1,
    });

    return updatedBank.getDataValue("coins");
};

const toOrdinal = (num: number) => {
    const rule = new Intl.PluralRules("en", { type: "ordinal" }).select(num);
    const suffixes: Record<Intl.LDMLPluralRule, string> = {
        zero: "th",
        one: "st",
        two: "nd",
        few: "rd",
        other: "th",
        many: "th",
    };
    return `${num}${suffixes[rule]}`;
};

const canAddCoin = async ({ author, channel, guildId, content }: Message) => {
    if (!/^\d+$/.test(content)) return;

    const guild = await GuildModel.findByPk(guildId!, {
        include: ["banks"],
    });

    const currentCount = guild?.getDataValue("count");

    return (
        !author.bot &&
        channel.type === "GUILD_TEXT" &&
        currentCount === Number(content) - 1 &&
        guild?.getDataValue("lastContributorId") !== author.id &&
        guild?.getDataValue("counterChannelId") === channel.id
    );
};

export const coinHandler = async (message: Message) => {
    if (!(await canAddCoin(message))) return;

    const { guildId, author, channelId } = message;

    try {
        const coins = await addCoin({
            guildId: guildId!,
            authorId: author.id,
            channelId,
        });

        message.reply(`You added your ${toOrdinal(coins)} :coin:`);
    } catch (err) {
        message.reply((err as Error).message.toString());
    }
};
