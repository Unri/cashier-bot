import type { SlashCommandBuilder } from "@discordjs/builders";
import type { BaseCommandInteraction } from "discord.js";

export interface Command {
    data: SlashCommandBuilder;
    execute: (interaction: BaseCommandInteraction) => Promise<void>;
}
