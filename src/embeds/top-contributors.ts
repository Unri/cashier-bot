import { MessageEmbed } from "discord.js";
import { BankModel } from "../db/models";
import type { Guild } from "discord.js";

const MAX_TOP_CONTRIBUTORS = 10;

const RANK_EMBED_COLOR = "#8A2BE2";

const rankingIcon = [
    "🥇",
    "🥈",
    "🥉",
    "4️⃣",
    "5️⃣",
    "6️⃣",
    "7️⃣",
    "8️⃣",
    "9️⃣",
    "🔟",
];

interface TopContributor {
    coins: number;
    contributors: string[];
}

export const embedTopContributors = async (guild: Guild) => {
    const topContributors = await sortContributorsOf(guild);
    return new MessageEmbed()
        .setTitle("🏆 TOP CONTRIBUTORS 🏆")
        .setColor(RANK_EMBED_COLOR)
        .addFields(
            topContributors
                .slice(0, MAX_TOP_CONTRIBUTORS)
                .map(({ coins, contributors }, i) => ({
                    name: `${rankingIcon[i]} ${contributors.join(", ")}`,
                    value: `> ${coins} :coin:`,
                }))
        );
};

const sortContributorsOf = async (guild: Guild): Promise<TopContributor[]> => {
    const banks = await BankModel.findAll({
        where: { guildId: guild.id },
    });

    if (banks.length === 0) return [];

    const members = guild.members.cache;

    const contributorsMap = banks
        .map((bank) => bank.toJSON())
        .filter(({ coins }) => coins > 0)
        .reduce<Record<number, string[]>>((contributors, contributor) => {
            if (!contributors[contributor.coins]) {
                contributors[contributor.coins] = [];
            }
            contributors[contributor.coins].push(
                members.get(contributor.userId)!.user.username
            );
            return contributors;
        }, {});

    return Object.entries(contributorsMap)
        .map(([coins, contributors]) => ({
            coins: Number(coins),
            contributors,
        }))
        .sort((a, b) => b.coins - a.coins);
};
