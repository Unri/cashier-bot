import fs from "fs";
import path from "path";
import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";
import { Collection } from "discord.js";
import { config } from "dotenv";
import type { Command } from "./types";
import type { RESTPostAPIChatInputApplicationCommandsJSONBody } from "discord-api-types/v9";

config();

const { DISCORD_TOKEN, DISCORD_CLIENT_ID, DISCORD_DEV_GUILD_ID } =
    process.env as Record<string, string>;

export const commands = fs
    .readdirSync(path.join(__dirname, "./commands"))
    .filter((file) => /\.[jt]s$/.test(file))
    .reduce((commandsCollection, file) => {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const command: Command = require(`./commands/${file}`)?.default;
        return commandsCollection.set(command.data.name, command);
    }, new Collection<string, Command>());

const rest = new REST({ version: "9" }).setToken(DISCORD_TOKEN);

if (process.env.NODE_ENV === "dev") {
    rest.put(
        Routes.applicationGuildCommands(
            DISCORD_CLIENT_ID,
            DISCORD_DEV_GUILD_ID
        ),
        {
            body: commands.map(({ data }) => {
                const builder =
                    data.toJSON() as RESTPostAPIChatInputApplicationCommandsJSONBody;
                return {
                    ...builder,
                    description: `${builder.description} (dev)`,
                };
            }),
        }
    )
        .then(() =>
            console.log("Successfully registered application commands.")
        )
        .catch(console.error);
} else {
    rest.put(Routes.applicationCommands(DISCORD_CLIENT_ID), {
        body: commands.map(({ data }) => data.toJSON()),
    })
        .then(() =>
            console.log("Successfully registered application commands.")
        )
        .catch(console.error);
}
