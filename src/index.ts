import { Client, Intents } from "discord.js";
import { config } from "dotenv";
import { sequelize } from "./db";
import { commands } from "./deploy-commands";
import { coinHandler } from "./handlers/coin-handler";
import { startingHandler } from "./handlers/starting-handler";
import { reportError } from "./report-error";

config();

new Client({
    intents: [
        Intents.FLAGS.GUILDS,
        Intents.FLAGS.GUILD_MESSAGES,
        Intents.FLAGS.GUILD_MEMBERS,
    ],
})
    .once("ready", async (client) => {
        await sequelize.sync();
        for (const guild of client.guilds.cache.values()) {
            if (guild.id) {
                await guild.members.fetch();
            }
        }
        startingHandler(client);
        console.log("Ready");
    })
    .on("interactionCreate", async (interaction) => {
        if (!interaction.isCommand()) return;

        if (!interaction.guild || interaction.channel?.isThread()) {
            return interaction.reply(
                "I don't take commands from DMs and threads"
            );
        }

        const { commandName } = interaction;
        const command = commands.get(commandName);

        if (!command) return;

        try {
            await command.execute(interaction);
        } catch (error) {
            reportError(interaction.client, error as Error);
            await interaction.reply({
                content: "There was an error while executing this command!",
                ephemeral: true,
            });
        }
    })
    .on("messageCreate", (message) => coinHandler(message))
    .login(process.env.DISCORD_TOKEN);
