import { SlashCommandBuilder } from "@discordjs/builders";
import { config } from "dotenv";
import { BankModel } from "../db/models";
import type { CommandInteraction } from "discord.js";

config();

const COIN_LIMIT = Number(process.env.COIN_LIMIT);

export default {
    data: new SlashCommandBuilder()
        .setName("coins")
        .setDescription("Display your remaining coins"),
    async execute(interaction: CommandInteraction) {
        const bank = await BankModel.findOne({
            where: {
                guildId: interaction.guildId,
                userId: interaction.user.id,
            },
        });

        interaction.reply(
            `You have ${
                COIN_LIMIT - (bank?.getDataValue("limitedCount") ?? 0)
            } :coin: left to spend`
        );
    },
};
