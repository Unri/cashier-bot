import { SlashCommandBuilder } from "@discordjs/builders";
import { GuildModel } from "../db/models";
import type { CommandInteraction } from "discord.js";

export default {
    data: new SlashCommandBuilder()
        .setName("count")
        .setDescription("Display the current count"),
    async execute(interaction: CommandInteraction) {
        const guild = await GuildModel.findByPk(interaction.guild!.id);

        return interaction.reply(
            `Current count: \`${guild?.getDataValue("count") ?? 0}\``
        );
    },
};
