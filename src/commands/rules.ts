import { SlashCommandBuilder } from "@discordjs/builders";

import { MessageEmbed } from "discord.js";
import { config } from "dotenv";
import type { CommandInteraction } from "discord.js";

config();
const { COIN_LIMIT } = process.env;

const rules = [
    "To add a coin, you have to increment the counter",
    "The counter cannot be incremented twice in a row by the same user",
    `The number of coins you can add is limited to ${COIN_LIMIT} per day`,
    "At the begining of each week, the top of contributors is displayed and all added coins are reset but not the counter",
    "The game is played only on one text channel",
];

const commands = {
    rules: "Display the rules and commands",
    coins: "Display your remaining coins",
    top: "Display the top contributors",
    count: "Display the current counter",
};

export const embedRules = new MessageEmbed()
    .setColor("#AF383C")
    .addField(
        "Rules",
        rules.map((rule, i) => `${i + 1}. ${rule}.`).join("\n\n")
    )
    .addField(
        "Commands",
        Object.entries(commands)
            .map(([name, description]) => `\`/${name}\`\n> ${description}`)
            .join("\n\n")
    );

export default {
    data: new SlashCommandBuilder()
        .setName("rules")
        .setDescription("Display the rules and commands"),
    execute(interaction: CommandInteraction) {
        return interaction.reply({ embeds: [embedRules] });
    },
};
