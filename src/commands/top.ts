import { SlashCommandBuilder } from "@discordjs/builders";
import { embedTopContributors } from "../embeds/top-contributors";
import type { BaseCommandInteraction, Guild } from "discord.js";

function assertIsDefined<T>(
    val: T | null | undefined
): asserts val is NonNullable<T> {
    if (val === undefined || val === null) {
        throw new Error(`Expected 'val' to be defined, but received ${val}`);
    }
}

export default {
    data: new SlashCommandBuilder()
        .setName("top")
        .setDescription("Prompt the top contributors"),
    async execute(interaction: BaseCommandInteraction) {
        assertIsDefined<Guild>(interaction.guild);

        return interaction.reply({
            embeds: [await embedTopContributors(interaction.guild)],
        });
    },
};
