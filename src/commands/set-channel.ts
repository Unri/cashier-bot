import { SlashCommandBuilder } from "@discordjs/builders";
import { GuildModel } from "../db/models";
import type { CommandInteraction, GuildChannel } from "discord.js";

export default {
    data: new SlashCommandBuilder()
        .setName("set-channel")
        .setDescription("Set the counter channel"),
    async execute(interaction: CommandInteraction) {
        const { guild, channel, member } = interaction;
        const user = interaction.guild!.members.cache.get(member!.user.id);
        const channelName = (channel as GuildChannel).name;

        if (interaction.channel!.type !== "GUILD_TEXT") return;

        if (!user?.permissions.has("ADMINISTRATOR")) {
            return interaction.reply({
                content:
                    "This command is restricted to administrators of this server",
                ephemeral: true,
            });
        }
        await GuildModel.upsert({
            id: guild!.id,
            counterChannelId: channel!.id,
        });
        return interaction.reply({
            content: `\`#${channelName}\` is set as Fund channel`,
            ephemeral: true,
        });
    },
};
