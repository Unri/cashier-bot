import { BankModel } from "../db/models";

export const resetLimitRoutine = () =>
    BankModel.update(
        {
            limitedCount: 0,
        },
        { where: {} }
    );
