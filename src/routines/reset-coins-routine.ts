import { BankModel, GuildModel } from "../db/models";
import { embedTopContributors } from "../embeds/top-contributors";
import type { Client, TextChannel } from "discord.js";

const RESET_DAY_INDEX = 1; // Monday

const resetCoins = () =>
    BankModel.update(
        {
            coins: 0,
        },
        { where: {} }
    );

export const reseCoinsRoutine = async (client: Client) => {
    if (new Date().getDay() === RESET_DAY_INDEX) {
        const guilds = await GuildModel.findAll();
        for (const guildData of guilds) {
            try {
                const channel = client.channels.cache.get(
                    guildData.getDataValue("counterChannelId")
                ) as TextChannel;
                channel.send({
                    embeds: [
                        await embedTopContributors(
                            client.guilds.cache.get(
                                guildData.getDataValue("id")
                            )!
                        ),
                    ],
                });
            } catch (e) {
                console.error(e);
            }
        }
        return resetCoins();
    }
};
