import { config } from "dotenv";
import type { Client } from "discord.js";

config();

const { REPORT_DISCORD_USER_ID } = process.env;

export const reportError = (client: Client, err: Error) => {
    console.error(err);
    client.users.cache
        .get(REPORT_DISCORD_USER_ID ?? "")
        ?.send(`\`\`\`${err.stack?.toString()}\`\`\``);
};
