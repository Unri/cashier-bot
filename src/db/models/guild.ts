import { DataTypes } from "sequelize";
import { sequelize } from "..";
import type { Bank } from "./bank";
import type { Optional, Model } from "sequelize";

export type Guild = {
    id: string;
    count: number;
    counterChannelId: string;
    lastContributorId: string;
    banks?: Bank[];
};

type GuildCreationTemplate = Optional<Guild, "lastContributorId" | "count">;

export const GuildModel = sequelize.define<Model<Guild, GuildCreationTemplate>>(
    "guild",
    {
        id: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        count: { type: DataTypes.INTEGER, defaultValue: 0 },
        counterChannelId: { type: DataTypes.STRING },
        lastContributorId: { type: DataTypes.STRING },
    },
    {
        tableName: "guild",
        underscored: true,
        timestamps: false,
    }
);
