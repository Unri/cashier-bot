import { BankModel } from "./bank";
import { GuildModel } from "./guild";

// Associations
GuildModel.hasMany(BankModel, { as: "banks" });

BankModel.belongsTo(GuildModel);

export { BankModel, GuildModel };
