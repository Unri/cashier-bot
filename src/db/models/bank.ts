import { DataTypes } from "sequelize";
import { sequelize } from "..";
import type { Model, Optional } from "sequelize";

export interface Bank {
    guildId: string;
    userId: string;
    coins: number;
    limitedCount: number;
}

export type BankCreationTemplate = Optional<Bank, "coins" | "limitedCount">;

export const BankModel = sequelize.define<Model<Bank, BankCreationTemplate>>(
    "bank",
    {
        guildId: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        userId: { type: DataTypes.STRING, primaryKey: true },
        coins: { type: DataTypes.INTEGER, defaultValue: 0 },
        limitedCount: { type: DataTypes.INTEGER, defaultValue: 0 },
    },
    {
        tableName: "bank",
        underscored: true,
        timestamps: false,
    }
);
