import { config } from "dotenv";
import { Sequelize } from "sequelize";

config();

const { DB_NAME, DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DATABASE_URL } =
    process.env;

export const sequelize = new Sequelize(
    DATABASE_URL ??
        `postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}`,
    {
        dialectOptions: {
            ssl: {
                require: true,
                rejectUnauthorized: false,
            },
        },
    }
);
